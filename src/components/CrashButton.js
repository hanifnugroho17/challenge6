import {TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import crashlytics from '@react-native-firebase/crashlytics';

const CrashButton = () => {
  return (
    <TouchableOpacity
      style={styles.Container}
      onPress={() => crashlytics().crash()}>
      <Icon style={styles.Icon} name={'close'} size={20} color="#ffff" />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Container: {
    marginLeft: 280,
    width: 30,
    height: 30,
    backgroundColor: '#000080',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});

export default CrashButton;
