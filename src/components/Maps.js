import {StyleSheet, Alert} from 'react-native';
import React, {useEffect, useState} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {request, check, PERMISSIONS, RESULTS} from 'react-native-permissions';

Geolocation.setRNConfiguration();

const Maps = () => {
  const [position, setPosition] = useState({
    lat: 37.78825,
    long: -122.4324,
  });

  const permission = async () => {
    try {
      await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      const result = await check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      result == RESULTS.DENIED
        ? Alert.alert(
            'WARNING',
            'Location Permission Denied! Please Allow Permission Request!',
          )
        : currentPosition();
    } catch (error) {
      Alert.alert('ERROR', error);
    }
  };

  const currentPosition = () => {
    Geolocation.getCurrentPosition(info => {
      setPosition({
        lat: info.coords.latitude,
        long: info.coords.longitude,
      });
    });
  };

  useEffect(() => {
    permission();
  }, []);

  return (
    <MapView
      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
      style={styles.map}
      region={{
        latitude: position.lat,
        longitude: position.long,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      }}
      showsUserLocation
      showsMyLocationButton
      followsUserLocation>
      <Marker
        coordinate={{
          latitude: -5.460993,
          longitude: 122.619805,
        }}
        title="Lipo Plaza Buton"
        description="Mall"></Marker>
      <Marker
        coordinate={{
          latitude: -5.523740,
          longitude: 122.567893,
        }}
        title="Pantai Nirwana"
        description="Beach"></Marker>
    </MapView>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    width: ' 100%',
    height: ' 100%',
  },
});

export default Maps;
