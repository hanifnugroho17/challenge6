import {View, Text, Alert, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import auth from '@react-native-firebase/auth';

const Header = ({navigation}) => {
  const user = auth().currentUser ? auth().currentUser.email : null;
  let username = user ? user.split('@')[0] : 'Annonymous!';

  const userSignOut = async () => {
    await auth().signOut();
    Alert.alert('SUCCESS', 'Sign Out Successful!');
    navigation.replace('Login');
  };

  const AnnonymousSignOut = async () => {
    Alert.alert('SUCCESS', 'Sign Out Successful!');
    navigation.replace('Login');
  };

  return (
    <View style={styles.Container}>
      <View style={styles.User}>
        <Text style={styles.Text}>Hello, {username}</Text>
      </View>
      <TouchableOpacity
        style={styles.Button}
        onPress={username == 'Annonymous!' ? AnnonymousSignOut : userSignOut}>
        <Icon
          style={styles.Icon}
          name={'logout-variant'}
          size={20}
          color="#fff"
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    zIndex: 1,
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
    width: '75%',
    justifyContent: 'space-between',
  },
  User: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 10,
    width: '80%',
  },
  Text: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,
    color: '#000',
    marginLeft: 8,
  },
  Button: {
    backgroundColor: '#ff0000',
    borderRadius: 50,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});

export default Header;
