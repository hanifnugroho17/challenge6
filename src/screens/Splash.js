import {StyleSheet, Text, View, StatusBar, Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';

const Splash = () => {
  const [user, setUser] = useState('');

  const navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = auth().onAuthStateChanged(user => {
      setUser(user);
      const isLogin = user ? 'BottomTabs' : 'Login';
      setTimeout(() => {
        navigation.replace(isLogin);
      }, 2000);
    });
    return unsubscribe;
  }, [user]);

  return (
    <View style={styles.Container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <Text style={styles.Title}>MAPS</Text>
      <Text style={styles.MyName}>Hanif Nugroho Jati</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#0B0C19',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: '40%',
    paddingBottom: '10%',
  },
  Title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 50,
    marginBottom: 40,
    color: '#ffff',
  },
  MyName: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    fontWeight: 'bold',
    color: '#ffff',
  },
});

export default Splash;
