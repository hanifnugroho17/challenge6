import React, {useEffect} from 'react';
import Scanner from '../components/Scanner';
import analytics from '@react-native-firebase/analytics';

const ScanQR = () => {
  const onScanQRScreenView = async () => {
    await analytics().logScreenView({
      screen_name: 'ScanQR',
      screen_class: 'ScanQR',
    });
  };

  useEffect(() => {
    onScanQRScreenView();
  }, []);

  return <Scanner />;
};

export default ScanQR;
