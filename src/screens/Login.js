import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  StatusBar,
  Alert,
  StyleSheet,
  Platform,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Input from '../components/Input';
import Button from '../components/Button';
import Direction from '../components/Direction';
import CrashButton from '../components/CrashButton';
import Fingerprint from '../components/Fingerprint';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, GoogleSigninButton} from '@react-native-google-signin/google-signin';
import analytics from '@react-native-firebase/analytics';
import TouchID from 'react-native-touch-id';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [touchID, setTouchID] = useState(true);

  GoogleSignin.configure({
    webClientId: 
      '995829192411-ds491p0j0679i3eu44h0dg61jb102uc1.apps.googleusercontent.com',
  });

  const isSupported = () => {
    TouchID.isSupported()
      .then(biometryType => {
        if (biometryType === true || biometryType === 'TouchID') {
          setTouchID(true);
        } else {
          setTouchID(false);
        }
      })
      .catch(error => {
        Alert.alert('ERROR', error);
      });
  };

  const handleLogIn = async () => {
    try {
      if (email != '' && password != '') {
        await auth().signInWithEmailAndPassword(email, password);
        await analytics().logEvent('user_login', {
          email: email,
        });
        Alert.alert('SUCCESS', 'Login Successful!');
        navigation.replace('BottomTabs');
      } else {
        Alert.alert('WARNING', 'Please input your email and password!');
      }
    } catch (error) {
      switch (error.code) {
        case 'auth/user-not-found':
          Alert.alert(
            'WARNING',
            "The email address you entered isn't connected to an account!",
          );
          break;
        case 'auth/invalid-email':
          Alert.alert('WARNING', 'That email address is invalid!');
          break;
        case 'auth/wrong-password':
          Alert.alert('WARNING', 'That password is uncorrect!');
          break;
        case 'auth/too-many-requests':
          Alert.alert('WARNING', 'Too many request, please try again later!');
          break;
      }
    }
  };

  const handleGoogleLogin = async () => {
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    // Sign-in the user with the credential
    await auth().signInWithCredential(googleCredential);
    Alert.alert('SUCCESS', 'Login Successful!');
    navigation.replace('BottomTabs');
  };

  const onLoginScreenView = async () => {
    await analytics().logScreenView({
      screen_name: 'Login',
      screen_class: 'Login',
    });
  };

  const _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = GoogleSignin.signIn()
      console.log (userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
      console.log
    }
  }

  useEffect(() => {
    onLoginScreenView();
    isSupported();
  }, []);

  return (
    <SafeAreaView style={styles.Main}>
      <KeyboardAvoidingView
        style={styles.Container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <StatusBar backgroundColor={'#ffff'} barStyle={'dark-content'} />
        <CrashButton />
        <Text style={styles.Title}>Login Screen</Text>
        <Input
          placeholder={'Email'}
          onChangeText={text => setEmail(text)}
        />
        <Input
          placeholder={'Password'}
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
        />
        <View style={styles.Button}>
          <Button
            caption={'LOG IN'}
            bgColor={'#00e100'}
            marginTop={10}
            onPress={handleLogIn}
          />
        </View>
        <Text style={styles.Separator}>- or -</Text>
        <View>
        <GoogleSigninButton
          style={{ width: 192, height: 48 }}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={_signIn}
        />
        </View>
        <Text style={styles.Fingerprint}> Fingerprint Login</Text>
        {touchID ? <Fingerprint navigation={navigation} /> : null}
        <Direction
          navigation={navigation}
          caption={"Don't have an account? "}
          page={'SignUp'}
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#0B0C19',
    justifyContent: 'center',
  },
  Container: {
    alignItems: 'center',
  },
  Title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 30,
    marginBottom: 40,
    color: '#ffff',
  },
  Button: {
    flexDirection: 'row',
  },
  Separator: {
    fontFamily: 'Poppins-Black',
    color: '#ffff',
    fontSize: 15,
    marginVertical: 10,
  },
  Fingerprint: {
    fontFamily: 'Poppins-Black',
    color: '#808080',
    fontSize: 14,
    marginTop: 10,
  },
});

export default Login;
